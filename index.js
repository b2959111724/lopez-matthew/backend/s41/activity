const express = require('express');

// Mongoose package allows the creation of schemas to model our data structure
// Also has access to a number of methods for manipulating our database.
const mongoose = require('mongoose');



const app = express();

const port = 3001;

// MONGOOSE CONNECTION

// Mongoose uses the 'connect' function to connect to the cluster in our MongoDB Atlas.
/*
	It takes 2 arguments:
		1. Connection string from MongoDB Atlas
		2. Object that contains the middleware/standard that MongoDB uses.
*/

mongoose.connect(`mongodb+srv://RupertRamos09:admin123@zuitt-batch224.re3gm0t.mongodb.net/s41?retryWrites=true&w=majority`, 
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);

// Initialize the mongoose connection to the MongoDB database by assigning 'mongoose.connection' to the 'db' variable.
let db = mongoose.connection


db.on('error', console.error.bind(console, "Connection Error"));
db.once('open', () => console.log('Connected to MongoDB!'))

// [SECTION] Mongoose Schemas

// Creating a Schema
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: 'Pending'
	}
});


// [SECTION] Models

const Task = mongoose.model('Task', taskSchema);

// [SECTION] Creating the Routes with logic

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// http://localhost:3001/tasks
// Create a single task - route
app.post('/tasks', (req, res) => {
	Task.findOne({name: req.body.name}).then((result, error) => {
		if(error) {
			return res.send(error)
		} else if (result != null && result.name == req.body.name) {
			return res.send('Duplicate task found!')
		} else {
			let newTask = new Task({
				name: req.body.name
			})

			newTask.save().then((savedTask, error) => {
				if (error) {
					return console.error(error)
				} else {
					return res.status(201).send("New task created");
				}
			})
		}
	})
});



// Get all post request - route
app.get('/tasks', (req, res) => {
	Task.find({}).then((result, err) => {
		if(err) {
			return console.log(err)
		} else {
			return res.status(200).json({data: result})
		}
	})
});


// S41 Activity Code here:

// User Schema/Model
const userSchema = new mongoose.Schema({
    username : String,
    password : String
})

const User = mongoose.model("User", userSchema);

// Register a user -route

// Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
	- If the user already exists in the database, we return an error
	- If the user doesn't exist in the database, we add it in the database
2. The user data will be coming from the request's body
3. Create a new User object with a "username" and "password" fields/properties
*/

// Route for creating a user
app.post("/signup", (req, res)=> {

	// Finds for a document with the matching username provided in the client/Postman
	User.findOne({ username : req.body.username }).then((result, err) => {

		// Check for duplicates
		if(result != null && result.username == req.body.username){

			return res.send("Duplicate username found");

		// No duplicates found
		} else {

			// If the username and password are both not blank
			if(req.body.username !== '' && req.body.password !== ''){

				// Create/instantiate a "newUser" from the "User" model
                let newUser = new User({
                    username : req.body.username,
                    password : req.body.password
                });
    
    			// Create a document in the database
                newUser.save().then((savedTask, saveErr) => {

                    // If an error occurred
                    if(saveErr){

                    	// Return an error in the client/Postman
                        return console.error(saveErr);

                    // If no errors are found
                    } else {

                    	// Send a response back to the client/Postman of "created"
                        return res.status(201).send("New user registered");

                    }

                })

            // If the "username" or "password" was left blank
            } else {

            	/// Send a response back to the client/Postman of "created"
                return res.send("BOTH username and password must be provided.");
            }			
		}
	})
})




// app.listen(port, () => console.log(`Server is running at port: ${port}.`));


// Listen to the port, meaning, if the port is accessed, we run the server

//An if statement is added here to be able to export the following app/server for checking.
if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));
}

module.exports = app;
 


